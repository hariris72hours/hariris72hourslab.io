#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import tweepy
import datetime


__version__ = "1.1"
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"


def main():
    """Script main function."""
    #  Unpacks variables
    api_key = os.environ["APP_TWITTER_API_KEY"]
    api_sec = os.environ["APP_TWITTER_API_SECRET"]
    token = os.environ["APP_TWITTER_ACCESS_TOKEN"]
    secret = os.environ["APP_TWITTER_ACCESS_SECRET"]
    round_hours = "APP_TWITTER_ROUND_HOURS" in os.environ
    #  Creates message
    total = datetime.timedelta(hours=72).total_seconds()
    orig_date = datetime.datetime(2019, 10, 18, 15, 50, tzinfo=datetime.timezone.utc)
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    elapsed = (now - orig_date).total_seconds()
    val = total - elapsed
    curr = abs(val)         # Uses absolute difference for display
    hh, mm, ss = 72, 0, 0   # Initialises variables
    if round_hours:
        hh, mm, ss = round(curr / 3600), 0, 0
    else:
        hh = int(curr / 3600)
        curr = curr % 3600
        print(curr, int(curr / 60), int(curr % 60))
        mm, ss = int(curr / 60), int(curr % 60)
    suffix = "overdue" if val < 0 else "remaining"
    msg = \
          "{0:0>2d}:{1:0>2d}:{2:0>2d} {3}\n"\
          "https://www.hariris72hours.com/".format(hh, mm, ss, suffix)
    #  Authenticates and sends message
    auth = tweepy.OAuthHandler(api_key, api_sec)
    auth.set_access_token(token, secret)
    api = tweepy.API(auth)
    api.verify_credentials()
    api.update_status(status=msg)

if __name__ == "__main__":
    main()
